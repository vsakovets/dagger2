package com.example.dagger2.component;

import com.example.dagger2.interfaces.RandomUserApplicationScope;
import com.example.dagger2.interfaces.RandomUsersApi;
import com.example.dagger2.module.PicassoModule;
import com.example.dagger2.module.RandomUsersModule;
import com.squareup.picasso.Picasso;

import dagger.Component;

//Component будет интерфейсом для всего графа зависимостей
//объявление только верхнеуровневых зависимостей
@RandomUserApplicationScope
@Component(modules = {RandomUsersModule.class, PicassoModule.class})
public interface RandomUserComponent {
    RandomUsersApi getRandomUserService();

    Picasso getPicasso();
}
