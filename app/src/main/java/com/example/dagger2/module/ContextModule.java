package com.example.dagger2.module;

import android.content.Context;

import com.example.dagger2.interfaces.RandomUserApplicationScope;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class ContextModule {

    Context context;

    public ContextModule(Context context) {
        this.context = context;
    }

    @RandomUserApplicationScope
    @Named("application_context")
    @Provides
    public Context context() {
        return context.getApplicationContext();
    }
}
