package com.example.dagger2.interfaces;

import com.example.dagger2.model.RandomUsers;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RandomUsersApi {
    @GET("api")
    //Параметры запроса добавляются с помощью аннотации @ Query к параметру метода. Они автоматически добавляются в конце URL-адреса.
    Call<RandomUsers> getRandomUsers(@Query("results") int size);
}
