package com.example.dagger2.interfaces;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

//аннотация @Scope говорит Dagger 2 создавать только единственный экземпляр,
// даже если DaggerComponent.build() вызывается многократно. Это заставляет зависимость работать как singleton.

//@Retention — это аннотация для обозначения точки отклонения использования аннотации.
// Она говорит о том, когда может быть использована аннотация.
// Например, с отметкой SOURCE аннотация будет доступна только в исходном коде и будет отброшена
// во время компиляции, с отметкой CLASS аннотация будет доступна во время компиляции, но не во
// время работы программы, с отметкой RUNTIME аннотация будет доступна и во время выполнения программы.

@Scope
@Retention(RetentionPolicy.CLASS)
public @interface RandomUserApplicationScope {

}
